<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.min.css">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.structure.min.css">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.theme.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

    <div class="row justify-content-center">
        <p id="result"></p>
    </div>
    <input type="text" id="siren">
    <button class="btn btn-primary" id="search">Recherche</button>
    <script src="jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <script src="jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script>
        $('#search').click(function() {
            var siren = $('#siren').val()
            console.log(siren);
            $.ajax({
                method: "GET",
                url: "https://data.opendatasoft.com/api/records/1.0/search/?dataset=sirene%40public&refine.siren=" + siren
            }).done(function(msg) {
                // Pour 1 seul siège.

                // var nom = msg.records['0'].fields.l1_normalisee
                // var libapen = msg.records['0'].fields.libapen
                // var libcom = msg.records['0'].fields.libcom
                // console.log(nom)
                // $('#result').html(nom + "<br/>" + libapen + "<br/> Ville : " + libcom )

                // Pour plusieurs sièges

                
                let i = 0
                for (var siege in msg.records) {
                    i = i + 1
                    var nom = msg.records[siege].fields.l1_normalisee
                    var libapen = msg.records[siege].fields.libapen
                    var libcom = msg.records[siege].fields.libcom
                    var enfant = "<p>Société n°" + i + "<br/>" + nom + "<br/>" + libapen + "<br/> Ville : " + libcom + "</p>"
                    $(enfant).appendTo('#result')
                }
            });

        })
    </script>
</body>

</html>