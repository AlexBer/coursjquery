<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.min.css">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.structure.min.css">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.theme.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <input type="email" id="email" placeholder="Email" required>
    <input type="password" id="password" placeholder="Mot de passe actuel" required>
    <input type="password" id="newpassword" placeholder="Nouveau mot de passe" required>
    <button class="btn btn-danger" id="send">
        Mettre à jour le mot de passe
    </button>
    <div id="result"></div>
    <script src="jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <script src="jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script>
        $(function() {

            $('#send').click(function() {

                var email = $('#email').val()
                var password = $('#password').val()
                var newpassword = $('#newpassword').val()
                result = {email: email, password: password, newpassword: newpassword}
                console.log(result)

                $.ajax({
                    type: "PUT",
                    url: "php/put.php",
                    data: result,
                    dataType: 'JSON'
                    
                }).done(function(response){
                    console.log(response)
                    $('#result').children('p').remove()
                    console.log(typeof(response.status))
                    if(response.status){
                        $("<p>Mot de passe modifié</p>").appendTo('#result')
                    }
                    else{
                        $("<p>Erreur</p>").appendTo('#result')
                    }
                })
            })
        });
    </script>
</body>
</html>