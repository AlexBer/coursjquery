<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/jquery-ui.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.structure.min.css">
</head>

<body>
    <div class="container-fluid">
        <div class="main-content">
            <select name="competitions" id="competitions"></select>
            <select name="equipes" id="equipes"></select>
        </div>
        <div id="result" style="margin-top:15px;border:1px solid red"></div>
    </div>
    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $.ajax({
                headers: {
                    'X-Auth-Token': '84a9a5d3ee434bcf9223094da2d2a165'
                },
                url: 'http://api.football-data.org/v2/competitions',
                dataType: 'JSON',
                type: 'GET',
            }).done(function(response) {
                $.each(response.competitions, function() {
                    $('<option value="' + this.id + '">' + this.name + ' - ' + this.area.name + '</option>').appendTo('#competitions');
                })

                $('#competitions').change(function() {
                    getTeams($(this).val());
                });
            });
        });

        function getTeams(id) {
            $('#equipes option').remove();
            $.ajax({
                headers: {
                    'X-Auth-Token': '84a9a5d3ee434bcf9223094da2d2a165'
                },
                url: 'http://api.football-data.org/v2/competitions/' + id + '/teams',
                dataType: 'JSON',
                type: 'GET',
            }).done(function(response) {
                for (c in response.teams) {
                    $('<option value="' + response.teams[c].id + '">' + response.teams[c].name + '</option>').appendTo('#equipes');
                }
                $('#equipes').off("change");
                $('#equipes').change(function() {
                    getTeam($(this).val());
                });
            });
        }

        function getTeam(id) {
            $('#result').html();
            $.ajax({
                headers: {
                    'X-Auth-Token': '84a9a5d3ee434bcf9223094da2d2a165'
                },
                url: 'http://api.football-data.org/v2/teams/' + id,
                dataType: 'JSON',
                type: 'GET',
            }).done(function(response) {
                for (c in response.squad) {
                    $('<p>' + response.squad[c].name + ' ' + response.squad[c].position + '</p>').appendTo('#result');
                }
            });
        }
    </script>
</body>

</html>