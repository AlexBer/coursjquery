<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.min.css">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.structure.min.css">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.theme.min.css">
    <div class="row ml-5">
    <select id="select">

    </select>
    <!-- <button id="valider">Valider</button> -->
    </div>
    <div class="row ml-5">
    <select id="selectequipe">
    </select>
    <!-- <button id="validerequipe">Valider</button> -->
    </div>
    <div id="equipe"></div>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>


    <script src="jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <script src="jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script>
        $.ajax({
            headers: {
                'X-Auth-Token': 'de772966c3584b38bf0bb023771fe0cb'
            },
            url: 'http://api.football-data.org/v2/competitions/',
            dataType: 'JSON',
            type: 'GET',
        }).done(function(response) {
            for (var competition in response.competitions) {
                var id = response.competitions[competition].id
                var name = response.competitions[competition].name
                var enfant = "<option value=' " + id + "'>" + name + "</option>"
                $(enfant).appendTo('#select')
            }
        });
        $('#select').change(function() {
            var idselect = $("#select option:selected").val()
            $('#selectequipe').children('option').remove();
            $.ajax({ 
                headers: {
                    'X-Auth-Token': 'de772966c3584b38bf0bb023771fe0cb'
                },
                url: 'http://api.football-data.org/v2/competitions/' + idselect + '/teams',
                dataType: 'JSON',   
                type: 'GET',
            }).done(function(response) {
                console.log(response)
                
                for (var teams of response.teams) {
                    console.log(teams.name)
                    var id = teams.id
                    var name = teams.name
                    var enfant = "<option value=' " + id + "'>" + name + "</option>"
                    $(enfant).appendTo('#selectequipe')
                }
            });
        })
        $('#selectequipe').change(function() {
            var idselectequipe = $("#selectequipe option:selected").val()
            $('#equipe').html('')
            $.ajax({
                headers: {
                    'X-Auth-Token': 'de772966c3584b38bf0bb023771fe0cb'
                },
                url: 'http://api.football-data.org/v2/teams/' + idselectequipe,
                dataType: 'JSON',
                type: 'GET',
            }).done(function(response) {
                for (var players of response.squad) {
                    var name = players.name
                    var enfant = "<p>" + name + "</p>"
                    $(enfant).appendTo('#equipe')
                }
            });
        })
        
    </script>
</body>

</html>