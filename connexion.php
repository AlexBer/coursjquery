<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.min.css">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.structure.min.css">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.theme.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    
    <input type="email" id="email" placeholder="Email" required>
    <input type="password" id="password" placeholder="Mot de passe" required>
    <button class="btn btn-primary" id="send">
        Connexion
    </button>
    <div id="result"></div>
    <script src="jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <script src="jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script>
        $(function() {
            $('#send').click(function() {

                var email = $('#email').val()
                var password = $('#password').val()
                var result = {email: email, password: password}
                $('#result').children('p').remove();
                $.ajax({
                    type: "POST",
                    url: "php/connexion.php",
                    dataType: "JSON",
                    data: result
                }).done(function(response) {
                    if(response.status == false){
                        console.log(response)
                        $('<p>Email ou mot de passe incorrect</p>').appendTo('#result')
                    }
                    else{
                        $('<p>Bienvenue ' + response.firstname + ' ' + response.name + '</p>').appendTo('#result')
                    }
                })

            })
        })
    </script>
</body>
</html>