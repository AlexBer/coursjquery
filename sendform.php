<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.min.css">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.structure.min.css">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.theme.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <input type="text" id="name" placeholder="Nom" required>
    <input type="text" id="firstname" placeholder="Prénom" required>
    <input type="number" id="age" placeholder="Âge" required>
    <input type="email" id="email" placeholder="Email" required>
    <input type="password" id="password" placeholder="Mot de passe" required>
    <button class="btn btn-primary" id="send" disabled="disabled">
        Inscription
    </button>
    <div id="dialog" title="Basic dialog" style="display:none;">
        <p>Vous avez déjà un compte. <a>Mot de passe oublié</a></p>
    </div>
    <script src="jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <script src="jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script>
        $(function() {

            // Vérifier si l'email n'existe pas en BDD

            $('#email').change(function() {
                var email = $('#email').val()
                $.ajax({
                method: "GET",
                url: "php/verifemail.php?email=" + email
            }).done(function(msg) {
                // console.log(typeof msg)

                // Je récupère un true or false de mon API (true si rowCount > 0 -> utilisateur deja inscrit)

                if(msg == 'true'){
                    $('#send').prop("disabled", true) // Désactivation du bouton envoyer
                    $('#dialog').dialog() // Ouverture d'un dialogue : Vous avez déjà un compte
                }

                else{
                    $('#send').prop("disabled",false) // Si l'utilisateur n'est pas inscrit, bouton utilisable
                }
            })
            })

            // Pour ahouter l'utilisateur (coté jquery -> API PHP)

            $('#send').click(function() {

                // récupération de la valeur des inputs, le hashage se fait coté php

                var name = $('#name').val()
                var firstname = $('#firstname').val()
                var age = $('#age').val()
                var email = $('#email').val()
                var password = $('#password').val()

                // console.log(name + ' ' + firstname + ' ' + age)

                // Création d'un objet result contenant les informations pour l'inscription

                result = {name: name, firstname: firstname, age: age, email: email, password: password}
                console.log(result)

                // Requête POST vers API PHP

                $.ajax({
                    type: "POST",
                    url: "php/api.php",
                    data: result,
                    dataType: JSON
                });
            })
        });
    </script>
</body>
</html>