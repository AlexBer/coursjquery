<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.min.css">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.structure.min.css">
    <link rel="stylesheet" href="jquery-ui-1.12.1.custom/jquery-ui.theme.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <table>
	<tr>
		<td>Liste des fruits :<br />
			<select id="liste_fruits" size="8" style="width:90px;">
				<option value="ananas">Ananas</option>
				<option value="banane">Banane</option>
				<option value="citron">Citron</option>
				<option value="fraise">Fraise</option>
				<option value="orange">Orange</option>
				<option value="pomme">Pomme</option>
				<option value="raisin">Raisin</option>
			</select>
		</td>
		<td>
			<button id="ajouter" disabled="disabled">Ajouter</button><br/>
			<button id="supprimer" disabled="disabled">Supprimer</button>
		</td>
		<td>Mon panier :<br />
			<select id="panier" size="8" style="width:90px;">
			</select>
		</td>
	</tr>
</table>
    <script src="jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <script src="jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script>

		// Ajouter un article au panier
        $('#liste_fruits').click(function() {
			$('#ajouter').prop("disabled",false)
		})
		$('#ajouter').click(function() {
			var value = $( "#liste_fruits option:selected" ).text()
			$('#panier').append(new Option(value, value));
		})

		// Supprimer un article du panier
		$('#panier').click(function() {
			$('#supprimer').prop("disabled",false)
		})
		$('#supprimer').click(function() {
			var valeur = $( "#panier option:selected" ).text()
			$("#panier option[value=valeur]").remove();
		})


    </script>
</body>

</html>