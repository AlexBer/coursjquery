<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\User;

class UsersController extends Controller
{

    /**
     * L'utilisateur actuel
     * @param type $id Identifiant de l'utilisateur
     */
    public function all(Request $request)
    {
        $users = User::all();
        return response()->json($users);
    }

}
